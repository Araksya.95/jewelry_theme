export const debounce = (fn, delay = 300) => {
	let timeOutId;
	return (...args) => {
		if (timeOutId) {
			clearTimeout(timeOutId);
		}
		timeOutId = setTimeout(() => {
			fn(...args);
		}, delay);
	};
};

export function throttle(callback, limit) {
	var waiting = false;
	return function () {
		if (!waiting) {
			callback.apply(this, arguments);
			waiting = true;
			setTimeout(function () {
				waiting = false;
			}, limit);
		}
	};
}

export function clickOutSide(ele, triggerEle, callback) {
	let element = typeof ele === 'string' && document.querySelector(ele);
	let triggerElement = typeof ele === 'string' && triggerEle;

	if (!element) return;

	function visibilityCheck(elem) {
		if (!(elem instanceof Element)) throw Error('DomUtil: elem is not an element.');
		const style = getComputedStyle(elem);
		if (style.display === 'none') return false;
		if (style.visibility !== 'visible') return false;
		if (style.opacity < 0.1) return false;
		if (
			elem.offsetWidth +
			elem.offsetHeight +
			elem.getBoundingClientRect().height +
			elem.getBoundingClientRect().width ===
			0
		) {
			return false;
		}
		const elemCenter = {
			x: elem.getBoundingClientRect().left + elem.offsetWidth / 2,
			y: elem.getBoundingClientRect().top + elem.offsetHeight / 2,
		};
		if (elemCenter.x < 0) return false;
		if (elemCenter.x > (document.documentElement.clientWidth || window.innerWidth)) return false;
		if (elemCenter.y < 0) return false;
		if (elemCenter.y > (document.documentElement.clientHeight || window.innerHeight)) return false;
		let pointContainer = document.elementFromPoint(elemCenter.x, elemCenter.y);
		do {
			if (pointContainer === elem) return true;
		} while ((pointContainer = pointContainer.parentNode));
		return false;
	}

	function isOutSide(target) {
		if (!target.closest(ele) && !target.closest(triggerElement)) {
			callback && callback(element, target);
		}
	}

	let handler = (e) => {
		let isVisible = visibilityCheck(element);
		if (isVisible) {
			isOutSide(e.target);
		}
		return;
	};
	document.addEventListener('click', handler);
}

export function initFnOnScroll(element, fn, beforeSectionTop = 300) {
	let windowHeight = window.innerHeight,
		elem = document.querySelector(element);

	//check if already in viewport;	
	callfnBeforeSection();

	window.addEventListener('scroll', throttle(callfnBeforeSection, 100));
	window.addEventListener('resize', debounce(callfnBeforeSection, 300))

	//set up function for delay call
	function callfnBeforeSection() {
		if (elem && !elem.classList.contains('inview')) {
			let currentPos = window.pageYOffset - windowHeight,
				elemPosition = elem.getBoundingClientRect().top,
				elemWindoBottomPos = (elemPosition + currentPos) - 300,
				windowYPos = window.scrollY;

			if (windowYPos > elemWindoBottomPos) {
				fn();
				elem.classList.add('inview');
			}
		}
	}
}

export function breakpointSwiper(fn, mediaQuery, debounceSpeed=0) {
	// More product carousel
	const breakpoint = window.matchMedia(mediaQuery);
	let tempSwiper;

	function initSwiper() {
		tempSwiper = fn();
	}

	if(breakpoint.matches == true ) {
		initSwiper();
	}


	function destroySliderIfDesktop() {
		if( breakpoint.matches == true ) {
			return initSwiper();
		}else if( breakpoint.matches == false ) {
			if( tempSwiper !== undefined ) tempSwiper.destroy(true, true);
		}
	}

	breakpoint.addEventListener('change', debounce(destroySliderIfDesktop, debounceSpeed));
}