import { $ as $$ } from 'dom7';
import Swiper, { Pagination, Navigation, Thumbs, EffectFade } from 'swiper';
import { breakpointSwiper, debounce } from '../utils';
//import Accordion from './_accordion';
// import OurStoryScience from './_our-story-science';
 import LazyLoad from 'vanilla-lazyload';

export default class Pdp {

	constructor() {
		this.init();
	}

	init() {
		//this.initPdpAccordion();
		this.initGallery();
		//this.initScienceTab();
		this.initMoreProductCarousel();
		this.initPdpSets();
		this.initReviewCarousel();
		//this.initIngredientsCarousel();
		//this.initPdpSetsCarousel();
	}
	productRecommend() {
		// Look for an element with class 'product-recommendations'
		var _this = this;
		var productRecommendationsSection = document.querySelector('.product-recommendations');
		if (productRecommendationsSection === null) {
			return;
		}
		// Read product id from data attribute
		var productId = productRecommendationsSection.dataset.productId;
		// Read limit from data attribute
		var limit = productRecommendationsSection.dataset.limit;
		// Build request URL
		var requestUrl =
			productRecommendationsSection.dataset.recommendationsUrl +
			'?section_id=product-recommendations&limit=' +
			limit +
			'&product_id=' +
			productId;
		// Create request and submit it using Ajax
		var request = new XMLHttpRequest();

		request.open('GET', requestUrl);
		request.onload = function () {
			if (request.status >= 200 && request.status < 300) {
				var container = document.createElement('div');
				container.classList.add('more-product');
				container.innerHTML = request.response;
				productRecommendationsSection.parentElement.innerHTML =
					container.querySelector('.product-recommendations').outerHTML;
				_this.initMoreProductCarousel();
				new LazyLoad({
					elements_selector: '.card__img',
				});
			}
		};
		request.send();
	}

	initGallery() {
		let thumbGallery;
		if ($$('#pdp-thumb-gallery').length) {
			thumbGallery = new Swiper('#pdp-thumb-gallery', {
				modules: [Navigation, Pagination],
				centeredSlides: true,
				centeredSlidesBounds: true,
				slidesPerView: 4,
				slidesPerGroup: 1,
				spaceBetween: 20,
				speed: 1000,
				watchOverflow: true,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				direction: 'vertical',
				//loop: true,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
		}

		new Swiper('#pdp-gallery', {
			modules: [Pagination, Navigation, Thumbs, EffectFade],
			slidesPerView: 1,
			effect: 'fade',
			loop: true,
			fadeEffect: {
				crossFade: true,
			},
			thumbs: {
				swiper: thumbGallery,
			},
		});
	}

	// pdp Bulnde
	initPdpSets() {
		new Swiper('#pdp-sets', {
			modules: [Pagination, Navigation],
			slidesPerView: 1,
			spaceBetween: 15,
			loop: false,
			pagination: {
				el: '.pdp-sets__progress',
				type: 'progressbar',
			},

			navigation: {
				nextEl: '.pdp-sets__nav--next',
				prevEl: '.pdp-sets__nav--prev',
			},
			breakpoints: {
				992: {
					slidesPerView: 1.5,
				},

				1400: {
					slidesPerView: 'auto',
				},
			},
			on: {
				afterInit: (swiper) => {
					new LazyLoad({
						container: swiper.el,
						unobserve_enter: true,
						cancel_on_exit: false,
					});
				},
			},
		});
	}

	initPdpAccordion() {
		new Accordion({
			element: '.accordion',
			head: '.accordion__head',	
			body: '.accordion__body',
			activeClass: 'open',
			oneOpen: true,
			callback : function() {
				this.element.forEach( acc => {
					if( acc.classList.contains('accordion--pair') ) {
						acc.classList.add('open', 'delay');
						acc.querySelector('.accordion__body').style.height= 'auto';
					}
				})
			}
		});
	}

	initPairSlider() {
		new Swiper('#pdp-slider', {
			modules: [Pagination],
			slidesPerView: 1.2,
			spaceBetween: 16,
			threshold: 10,
			loop: true,
			observeParents: true,
			pagination: {
				el: '.swiper-progress',
				type: 'progressbar',
			},
			breakpoints: {
				768: {
					slidesPerView: 1.7,
					spaceBetween: 28,
				},
			},
			on: {
				afterInit: (swiper) => {
					new LazyLoad({
						container: swiper.el,
						unobserve_enter: true,
						cancel_on_exit: false,
					});
				},
			},
		});
	}

	//feature carousel
	initFeatureCarousel() {
		new Swiper('#feature-carousel', {
			modules: [Pagination, Navigation],
			slidesPerView: 2,
			loop: true,
			breakpoints: {
				992: {
					slidesPerView: 4,
				},
			},
			pagination: {
				el: '.pdp-feature__slider-progress',
				type: 'progressbar',
			},

			navigation: {
				nextEl: '.pdp-feature__slider-nav--next',
				prevEl: '.pdp-feature__slider-nav--prev',
			},
		});
	}

	//ingredients
	initIngredientsCarousel() {
		
		let ingredientSlider = new Swiper('#ingredients-carousel', {
			modules: [Navigation],
			slidesPerView: 'auto',
			loop: true,
			threshold: 10,
			breakpoints: {
				992: {
					spaceBetween: 30,
				},
			},
			navigation: {
				nextEl: '.ingredients-slider-nav--next',
				prevEl: '.ingredients-slider-nav--prev',
			},
			on: {
				slideChangeTransitionEnd: function () {
					document.querySelectorAll('.ingredients__link').forEach((link) => {
						if (link.nextElementSibling.classList.contains('ingredients__content--active')) {
							link.click();
						}
					});
				}
			}
		});

		document.querySelectorAll('.ingredients__link').forEach((ingredient) => {
			ingredient.addEventListener('click', toggleTextIngredient);
		});

		let previousHeight = 0;

		function toggleTextIngredient() {
			event.preventDefault();
			let targetElement = event.target,
				accBody = targetElement.closest('.accordion__body');
				
			updateLinkText();
			targetElement.nextElementSibling.classList.toggle('ingredients__content--active');
			ingredientSlider.update();
			if( targetElement.nextElementSibling.classList.contains('ingredients__content--active') ) {
				previousHeight = accBody.style.height;
				accBody.style.height = `${accBody.scrollHeight}px`;
				accBody.classList.add('remove-transition');
			}else {
				accBody.style.height = previousHeight;
				accBody.classList.remove('remove-transition');
				previousHeight = 0;
			}
		}

		function updateLinkText() {
			let initialText =
				event.target.innerText == 'See full ingredients +'
					? 'Hide full ingredients -'
					: 'See full ingredients +';
			event.target.textContent = initialText;
		}
	}

	//init reveiw carousel
	initReviewCarousel() {
		new Swiper('#reviews-carousel', {
			modules: [Pagination, Navigation],
			slidesPerView: 1.2,
			spaceBetween: 20,
			loop: true,
			breakpoints: {
				992: {
					slidesPerView: 3,
					spaceBetween: 38,
				},
			},

			pagination: {
				el: '.reviews__slider-progress',
				type: 'progressbar',
			},

			navigation: {
				nextEl: '.reviews__slider-nav--next',
				prevEl: '.reviews__slider-nav--prev',
			},
		});
	}

	// More product carousel
	initMoreProductCarousel() {
		const breakpoint = '(max-width: 991.98px)';

		function initSwiper() {
			return new Swiper('#more-product-carousel', {
				slidesPerView: 1.2,
				spaceBetween: 13,
				loop: true,
				modules: [Pagination],
				pagination: {
					type: 'progressbar',
					el: '.upsell-slider__progress',
				},
			});
		}

		breakpointSwiper(initSwiper, breakpoint, 300);
	}

	// modal carousel
	initModalCarousel() {
		let slides = [...document.querySelectorAll('.lightbox__grid')],
			currentSlideCount = document.querySelector('.swiper-pagination-current'),
			totalSlideCount = document.querySelector('.swiper-pagination-total');

		new Swiper('#lightbox-carousel', {
			modules: [Navigation],
			slidesPerView: 1,
			loop: true,
			navigation: {
				nextEl: '.lightbox__carousel-nav--next',
				prevEl: '.lightbox__carousel-nav--prev',
			},
			on: {
				afterInit: (swiper) => {
					currentSlideCount.textContent = 1;
					totalSlideCount.textContent = slides.length;
				},
				slideChangeTransitionEnd: (swiper) => {
					currentSlideCount.textContent = swiper.realIndex + 1;
				},
			},
		});

		document.querySelectorAll('.lightbox-gallery-thumb .swiper').forEach((thumb, index) => {
			let lightBoxThumb = new Swiper(thumb, {
				slidesPerView: 'auto',
				watchSlidesProgress: true,
			});

			new Swiper(document.querySelectorAll('.lightbox-gallery__carousel .swiper')[index], {
				modules: [Thumbs, EffectFade],
				slidesPerView: 1,
				loop: true,
				// effect: 'fade',
				// fadeEffect: {
				// 	crossFade: true,
				// },
				on: {
					afterInit: (swiper) => {
						new LazyLoad({
							container: swiper.el,
							unobserve_enter: true,
							cancel_on_exit: false,
						});
					},
				},
				thumbs: {
					swiper: lightBoxThumb,
				},
			});
		});
	}

	initScienceTab() {
		new OurStoryScience();
	}

	initRatingAutoScroll() {
		document.querySelector('#rating').addEventListener('click', (e) => {
			e.preventDefault();
			this.gotoSection('#reviews');
		});
	}

	stickyTopBar() {
		let stickyBarLinks = document.querySelectorAll('.sticky-nav__link'),
			stickyHeader = document.querySelector('.sticky-addcart');

		stickyBarLinks.forEach((elem) => {
			elem.addEventListener('click', (e) => {
				e.preventDefault();
				stickyBarLinks.forEach((elem) => elem.classList.remove('sticky-nav__link--active'));
				elem.classList.add('sticky-nav__link--active');
				this.gotoSection(elem.getAttribute('href'));
			});
		});

		if (stickyHeader) {
			window.addEventListener('scroll', addClassToActiveSection);
			window.addEventListener('scroll', showStickybar, { passive: true });
		}

		function addClassToActiveSection() {
			let links = document.querySelectorAll('.sticky-nav__link'),
				stickyMainHeader = document.querySelector('.header__sticky').scrollHeight;

			let stickyTopBar = document.querySelector('.sticky-addcart'),
				stickyTopBarHeight = stickyTopBar ? stickyTopBar.scrollHeight + stickyMainHeader - 2 : 0;

			stickyBarLinks.forEach((link) => {
				let sectionId = link.getAttribute('href');

				if (sectionId) {
					let section = document.querySelector(sectionId);
					if (!section) return;
					let sectionTop = section.getBoundingClientRect().top - stickyTopBarHeight;

					if (sectionTop <= 0) {
						links.forEach((elem) => elem.classList.remove('sticky-nav__link--active'));
						link.classList.add('sticky-nav__link--active');
					}
				}
			});
		}

		function showStickybar() {
			let sectionTop = document.querySelector('.pdp__info').getBoundingClientRect().top;
			if (sectionTop <= 0) {
				stickyHeader.classList.add('sticky-addcart--sticky');
			} else {
				stickyHeader.classList.remove('sticky-addcart--sticky');
			}
		}
	}

	gotoSection(elem) {
		let stickyTopBar = document.querySelector('.sticky-addcart'),
			stickyMainHeader = document.querySelector('.header__sticky').scrollHeight,
			stickyTopBarHeight = stickyTopBar ? stickyTopBar.scrollHeight + stickyMainHeader - 5 : 0,
			section = document.querySelector(elem);

		if (section) {
			window.scroll({
				top: section.offsetTop - stickyTopBarHeight,
				left: 0,
				behavior: 'smooth',
			});
		}
	}

	selectPurchaseOrsubscribe() {
		let buttons = document.querySelectorAll('.pdp-details-btn'),
			delivery = document.querySelector('.delivery-frequency');

		buttons.forEach((elem, index) => {
			elem.addEventListener('click', function () {
				let type = this.getAttribute('data-type');
				buttons.forEach((elem) => elem.classList.remove('active'));
				this.classList.add('active');
				if (type == 'subscribe') {
					delivery.style.display = 'flex';
				} else {
					delivery.style.display = 'none';
				}
			});
		});
	}

	initNotifyModal() {
		$$('.js-notify').on('click', (e) => {
			e.preventDefault();
			$$('.out-of-stock').fadeIn();
		});

		$$('.modal__close').on('click', (e) => {
			e.preventDefault();
			$$('.out-of-stock').fadeOut();
		});
	}

	updateVariationTitle() {
		let variationTitle = $$('.shade-menu__label');

		$$('.color-variation__item input').each((input) => {
			$$(input).on('change', updateTitle);
		});

		function updateTitle() {
			let title = $$(this).attr('data-title');
			variationTitle.text(title);
		}
	}

	playVideoOnClick() {
		$$('.pdp-blog__play-btn').on('click', toggleVideo);
		$$(document).on('click', '.pdp-blog__video', toggleVideo);

		function toggleVideo() {
			let button = event.target.classList.contains('pdp-blog__play-btn')
					? event.target
					: event.target.closest('.pdp-blog__thumb').querySelector('.pdp-blog__play-btn'),
				video = event.target.closest('.pdp-blog__thumb').querySelector('video');

			if (video.paused) {
				video.play();
				button.style.display = 'none';
			} else {
				video.pause();
				button.style.display = 'flex';
			}
		}
	}

	initReviewPopup() {
		document.querySelectorAll('.light-box-trigger').forEach((element) => {
			element.addEventListener('click', () => {
				$$('.lightbox').fadeIn();
			});
		});
	}
}
