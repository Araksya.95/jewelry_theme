//throttle
const throttle = (func, delay) => {
    let last = 0
    return (...args) => {
        const now = new Date().getTime()
        if (now - last < delay) {
            return
        }
        last = now
        func(...args)
    }
}

//check window resize
const checkWindowResize = (cb, delay) => {
    let lastWindowSize = window.innerWidth,
        timerId = null
    return (...args) => {
        const currentWindowSize = window.innerWidth
        if (currentWindowSize != lastWindowSize) {
            clearTimeout(timerId)
            timerId = setTimeout(() => {
                cb(...args)
            }, delay)
        }
        lastWindowSize = currentWindowSize
    }
}

// check breakpoint
const checkBreakPoint = (brekapoint) => {
    const mediaBreakPoint = window.matchMedia(`(${brekapoint})`)

    if (mediaBreakPoint.matches) {
        return true
    } else {
        return false
    }
}

export {throttle, checkWindowResize, checkBreakPoint}
